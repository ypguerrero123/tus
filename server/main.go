package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/tus/tusd/pkg/filestore"
	tusd "github.com/tus/tusd/pkg/handler"
	"github.com/tus/tusd/pkg/s3store"
	"net/http"
	//"github.com/snikch/goodman/hooks"
)

func main() {
	fmt.Println("Init server")
	// Create a new FileStore instance which is responsible for
	// storing the uploaded file on disk in the specified directory.
	// This path _must_ exist before tusd will store uploads in it.
	// If you want to save them on a different medium, for example
	// a remote FTP server, you can implement your own storage backend
	// by implementing the tusd.DataStore interface.

	// S3 access configuration
	s3Config := &aws.Config{
		Region:           aws.String("us-east-1"),
		Credentials:      credentials.NewStaticCredentials("AKIAJBJ7O6ACMJOJNXJQ", "FIrgQahGnT4eP9pXaEzXHgvzZQhnmHleffdTvgYU", ""),
		Endpoint:         aws.String(""),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
		MaxRetries:       aws.Int(3),
	}

	sess := s3.New(session.Must(session.NewSession()), s3Config)

	/*result, err := sess.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		panic(fmt.Errorf("Unable to list buckets: %s", err))
	}

	for _, b := range result.Buckets {
		fmt.Println("* %s", aws.StringValue(b.Name))
	}

	fmt.Println("Buckets:")
	*/

	// Setting up the store storage
	storeS3 := s3store.New("cdrive-storage-test-0", sess)
	storeS3.ObjectPrefix = "xid:krn:"

	fmt.Println("Store success")

	// A storage backend for tusd may consist of multiple different parts which
	// handle upload creation, locking, termination and so on. The composer is a
	// place where all those separated pieces are joined together. In this example
	// we only use the file store but you may plug in multiple.
	composer := tusd.NewStoreComposer()
	storeS3.UseIn(composer)

	storeFile := filestore.FileStore{
		Path: "./uploads",
	}
	storeFile.UseIn(composer)

	fmt.Println("Composer new storage success")

	// Create a new HTTP handler for the tusd server by providing a configuration.
	// The StoreComposer property must be set to allow the handler to function.
	handler, err := tusd.NewHandler(tusd.Config{
		BasePath:                "/files/",
		StoreComposer:           composer,
		NotifyCompleteUploads:   true,
		NotifyTerminatedUploads: true,
		NotifyUploadProgress:    true,
		NotifyCreatedUploads:    true,
		PreUploadCreateCallback: func(hook tusd.HookEvent) error {
			storeS3.Bucket = "cdrive-storage-test-1" // event.Upload.MetaData.bucket
			storeFile.Path = "./uploads2" // event.Upload.MetaData.path_drive

			fmt.Println("Pre upload %s created\n", hook.Upload)

			storeS3.UseIn(composer)
			storeFile.UseIn(composer)

			return nil
		},
	})
	if err != nil {
		panic(fmt.Errorf("Unable to create handler: %s", err))
	}



	go func() {
		for {
			event := <-handler.CreatedUploads
			fmt.Println("Upload %s created\n", event.Upload.ID)
		}
	}()

	go func() {
		for {
			event := <-handler.UploadProgress
			fmt.Println("Upload %s UploadProgress\n", event.Upload.ID)
		}
	}()

	// Start another goroutine for receiving events from the handler whenever
	// an upload is completed. The event will contains details about the upload
	// itself and the relevant HTTP request.
	go func() {
		for {
			event := <-handler.CompleteUploads
			fmt.Println("Upload %s finished\n", event.Upload)
			fmt.Println(storeS3.Bucket)
			fmt.Println(storeFile.Path)
		}
	}()

	// Right now, nothing has happened since we need to start the HTTP server on
	// our own. In the end, tusd will start listening on and accept request at
	// http://localhost:8010/files
	http.Handle("/files/", http.StripPrefix("/files/", handler))
	err = http.ListenAndServe(":8010", nil)
	if err != nil {
		panic(fmt.Errorf("Unable to listen: %s", err))
	}
}
